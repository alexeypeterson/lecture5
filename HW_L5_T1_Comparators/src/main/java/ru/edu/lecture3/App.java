package ru.edu.lecture3;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        CustomDigitСomparator customDigitСomparator = new CustomDigitСomparator();

        int a = 15;
        int b = 12;
        int c = -25;
        int aa = 14;
        int ba = 11;
        int ca = 2;

        List<Integer> intList = new ArrayList<>();
        intList.add(a);
        intList.add(b);
        intList.add(c);
        intList.add(aa);
        intList.add(ba);
        intList.add(ca);
        System.out.println("Before: " + intList);
        intList.sort(customDigitСomparator);
        System.out.println("After: " + intList);
    }
}
