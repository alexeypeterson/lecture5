package ru.edu.lecture3;

public class Person implements Comparable<Person>{
    private String name;
    private String city;
    private int age;


    public Person(String name, String city, int age) {
        if (name == null) {
            this.name = "Name not set";
        }else {
            this.name = name;
        }

        if (city == null) {
            this.city = "City not set";
        }else {
            this.city = city;
        }

        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }

    /**
     *  сначала по полю city, а затем по полю name
     */
    @Override
    public int compareTo(Person other) {

        if (this.city == null || this.name == null || other == null){
            throw new IllegalArgumentException("Arguments can't be NULL");
        }

        int result = this.getCity().compareToIgnoreCase(other.getCity());

        if (result == 0){
            return this.getName().compareToIgnoreCase(other.getName());
        }
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }


}

