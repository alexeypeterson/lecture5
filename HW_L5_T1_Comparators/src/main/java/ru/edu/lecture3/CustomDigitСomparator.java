package ru.edu.lecture3;

import java.util.Comparator;

public class CustomDigitСomparator implements Comparator<Integer> {

    /**
     * Сначала четные числа, затем нечетные.
     */
    @Override
    public int compare(Integer lhs, Integer rhs) {
        if (lhs == null || rhs == null) {
            throw new IllegalArgumentException("Arguments can't be NULL");
        }


         if (lhs % 2 == 0 && rhs % 2 != 0) {
            return -1;
        } else if (lhs % 2 != 0 && rhs % 2 == 0) {
            return 1;
        }
         return 0;
    }
}
