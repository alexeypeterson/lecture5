package ru.edu.lecture3;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CustomDigitСomparatorTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    /**
     * Проверка метода Compare.
     */
    @Test
    public void testCompare() {
        List<Integer> intListForTest = new ArrayList<>();
        CustomDigitСomparator customDigitСomparator = new CustomDigitСomparator();
        int a = 15;
        int b = 12;

        intListForTest.add(a);
        intListForTest.add(b);


        int actualBefore = intListForTest.get(0);
        Assert.assertEquals(15, actualBefore);
        intListForTest.sort(customDigitСomparator);
        int actualAfter = intListForTest.get(0);
        Assert.assertEquals(12, actualAfter);
    }


    /**
     * Проверка метода Compare на выброс исключения.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCompare_Error() {
        List<Integer> intListForTest = new ArrayList<>(2);
        CustomDigitСomparator customDigitСomparator = new CustomDigitСomparator();
        int a = 1;
        int b = 2;

        intListForTest.add(a);
        intListForTest.add(b);

        intListForTest.set(1,null);

        intListForTest.sort(customDigitСomparator);
    }
}