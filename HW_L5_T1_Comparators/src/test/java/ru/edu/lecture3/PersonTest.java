package ru.edu.lecture3;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PersonTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void compareTo() {
        Person person1 = new Person("Alexey", "Ekb", 33);
        Person person2 = new Person("Alexey", "Alanya", 29);
        Person person3 = new Person("Bruce", "Alanya", 19);


        List<Person> personListForTest = new ArrayList<>();
        personListForTest.add(person1);
        personListForTest.add(person2);
        personListForTest.add(person3);


        Assert.assertEquals(person1,personListForTest.get(0));
        Assert.assertEquals(person2,personListForTest.get(1));
        Assert.assertEquals(person3,personListForTest.get(2));

        personListForTest.sort(Person::compareTo);

        Assert.assertEquals(person1,personListForTest.get(2));
        Assert.assertEquals(person2,personListForTest.get(0));
        Assert.assertEquals(person3,personListForTest.get(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void compareTo_Error() {
        Person person1 = new Person("Alexey", "Ekb", 33);
        Person person2 = new Person("Alexey", "Alanya", 29);
        Person person3 = new Person("Bruce", "Alanya", 19);

        List<Person> personListForTest = new ArrayList<>();
        personListForTest.add(person1);
        personListForTest.add(person2);
        personListForTest.add(person3);

        personListForTest.set(0,null);

        personListForTest.sort(Person::compareTo);
    }

}