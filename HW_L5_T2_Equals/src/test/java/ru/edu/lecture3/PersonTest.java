package ru.edu.lecture3;

import org.junit.Assert;
import org.junit.Test;



public class PersonTest {


    /**
     * Проверка метода Equals
     */
    @Test
    public void testEquals() {
        Person person1 = new Person("Alexey","EkaterinBURG", 29);
        Person person2 = new Person("ALEXEY","Ekaterinburg", 29);
        Person person3 = new Person("Svetlana","Tula", 30);

        Assert.assertEquals(equals(person1), equals(person2));
        Assert.assertFalse(person1.equals(person3));


    }

    /**
     * Проверка метода HashCode
     */
    @Test
    public void testHashCode() {
        Person person1 = new Person("Alexey","Ekaterinburg", 29);
        Person person2 = new Person("Alexey","Ekaterinburg", 29);
        Person person3 = new Person("Svetlana","Tula", 30);

        Assert.assertEquals(person1.hashCode(), person2.hashCode());
        Assert.assertNotEquals(person1.hashCode(), person3.hashCode());

    }


}