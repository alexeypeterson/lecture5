package ru.edu.lecture3;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

public class DigitHandlerTest extends TestCase {

    /**
     * Проверка метода Equals
     */
    @Test
    public void testEquals() {
        DigitHandler digitHandler1 = new DigitHandler(10);
        DigitHandler digitHandler2 = new DigitHandler(15);
        DigitHandler digitHandler3 = new DigitHandler(10);

        Assert.assertEquals(digitHandler1, digitHandler3);
        Assert.assertNotEquals(digitHandler1, digitHandler2);
    }

    /**
     * Проверка метода HashCode
     */
    @Test
    public void testHashCode() {
        DigitHandler digitHandler1 = new DigitHandler(10);
        DigitHandler digitHandler2 = new DigitHandler(15);
        DigitHandler digitHandler3 = new DigitHandler(10);

        Assert.assertEquals(digitHandler1.hashCode(), digitHandler3.hashCode());
        Assert.assertNotEquals(digitHandler1.hashCode(), digitHandler2.hashCode());

    }
}