package ru.edu.lecture3;

public class App {
    public static void main(String[] args) {
        Person person1 = new Person("Alexey","Ekb", 29);
        Person person2 = new Person("Alexey","Ekb", 29);
        Person person3 = new Person("Svetlana","Tula", 30);

        System.out.println(person1);
        System.out.println("person1 name: " + person1.getName());
        System.out.println("person1 equals person2: " + person1.equals(person2));
        System.out.println("person1 equals person3: " + person1.equals(person3));
        System.out.println("person1 hashCode: " + person1.hashCode());
        System.out.println("person2 hashCode: " + person2.hashCode());
        System.out.println("person3 hashCode: " + person3.hashCode());
    }
}
